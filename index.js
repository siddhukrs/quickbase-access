var config = require('./config.js');
var QuickBase = require('quickbase');

var quickbase = new QuickBase({
	realm: config.realm,
	appToken: config.appToken
});

var authPromise = quickbase.api('API_Authenticate', {
    username: config.username,
    password: config.password
});

const recordsAtATime = 10;
const breakdownType = {
    AGE: 'age'
};
const outputFormat = {
    CSV: 'csv'
};

authPromise.then((result) => {
	return quickbase.api('API_DoQuery', {
		dbid: config.sourceDbId,
		clist: 'a',
		options: 'num-' + recordsAtATime,
	}).then((result) => {
            generateNewlyEnrolledReport(breakdownType.AGE, outputFormat.CSV);
	});
});

var generateNewlyEnrolledReport = function (breakdown, oFormat) {
    // The IDs of relevant fields
    const fieldId = {
        enrollmentStatus: '113',
        coverageStatus:
        [
            {
                age: '76',
                currentCoverage: '79'
            },
            {
                age: '124',
                currentCoverage: '126'
            },
            {
                age: '133',
                currentCoverage: '135'
            },
            {
                age: '142',
                currentCoverage: '144'
            },
            {
                age: '151',
                currentCoverage: '153'
            },
            {
                age: '160',
                currentCoverage: '162'
            },
            {
                age: '169',
                currentCoverage: '171'
            },
            {
                age: '178',
                currentCoverage: '180'
            }
        ]
    };

    let clist;

    authPromise.then(
        result => {
            clist = fieldId.enrollmentStatus;
            for (let fid of fieldId.coverageStatus) {
                clist += `.${fid.age}.${fid.currentCoverage}`;
            }


            // Query for the set of Enrolled households
            return quickbase.api(
                'API_DoQuery',
                {
                    dbid: config.sourceDbId,
                    clist,
                    query: `{'${fieldId.enrollmentStatus}'.CT.'Enrolled'}`,
                }
            );
        }
    ).then(
        result => {
            const records = result.table.records;

            // Saves the name of queried fields for notes
            const fieldsExamined = [];
            const clistArr = clist.split('.');
            result.table.fields.forEach(fieldObject => {
                if (clist.includes(`${fieldObject.id}`)) {
                    fieldsExamined.push(fieldObject.label);
                }
            });

            // The data structure for parsed data
            const outputData = {
                title: 'Enrolled by Age',
                columns: [ 'Age', 'Enrolled Count' ],
                data: {},
                notes: [
                    'Fields Examined: ' + fieldsExamined.join(' - ')
                ]
            };

            let numEnrolledHouseholds = 0;

            // Parse the queried results
            result.table.records.forEach(
                // Parse each row
                recordObject => {
                    numEnrolledHouseholds++;
                    // Parse each column
                    fieldId.coverageStatus.forEach(
                        // The logic to determine whether each person specified
                        // in the row was newly ensured
                        coverageStatusId => {
                            //if (recordObject[coverageStatusId.currentCoverage] === "Uninsured") {
                                if (recordObject[coverageStatusId.age] !== undefined) {
                                const updateEntry = outputData.data[recordObject[coverageStatusId.age]]
                                    || outputData.data[recordObject[coverageStatusId.age]] === 0;

                                // Starts or increments the count of
                                // newly ensured by age
                                updateEntry
                                    ? outputData.data[recordObject[coverageStatusId.age]]++
                                    : outputData.data[recordObject[coverageStatusId.age]] = 1;
                                }
                            //}
                        }
                    );
                }
            );

            // Stores the number of enrolled households to sanity check
            outputData.notes.push('Enrolled Households: ' + numEnrolledHouseholds);

            // Gets the number newly enrolled people for notes
            let toOutputNewlyEnrolled = 0;
            for (let key in outputData.data) {
                toOutputNewlyEnrolled += outputData.data[key];
            }
            outputData.notes.push('Newly-enrolled People: ' + toOutputNewlyEnrolled);

            // Output to console
            if (breakdown === breakdownType.AGE) {
                if (oFormat === outputFormat.CSV) {
                    console.log(outputData.title);
                    console.log(outputData.columns.join(','));
                    Object.entries(outputData.data).forEach(row => console.log(row.join(',')));
                    console.log('');
                    console.log('Notes');
                    outputData.notes.forEach(note => console.log(note));
                } else {
                    console.log('Output file format not supported.');
                }
            } else {
                console.log('Data breakdown type not supported.');
            }
        }
    );
}

var readRecordFromSource = function () {

};

var addRecordToDestination = function (sourceFields) {
	// generate and cache a unique numeric ID - HouseholdID
	quickbase.api('API_AddRecord', {
		dbid: config.destinationDbId,
		fields: sourceFields, //array of {fid: '', value:''}
		disprec: 1,
	});
	// Add record as is with household members empty and the new HouseholdID field
	
	// Add each household member as new entry with same and the new HouseholdID field
};

var createOtherPeopleRecords = function (record) {
	
};

